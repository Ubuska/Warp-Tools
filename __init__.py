import sys
import importlib

bl_info = {
    "name": "Warp Tools",
    "description": "Handy collection of tools for modelling.",
    "author": "Peter Gubin",
    "version": (0, 0, 0),
    "blender": (2, 79, 0),
    "location": "View3D - 'Q' key gives a menu in Object, Edit, and Sculpt modes.",
    "warning": "",
    "wiki_url": "",
    "tracker_url": "",
    "category": "3D View"}

#modulesNames = ['panels.quick_object_mode',
 #               'panels.quick_edit_mode',
 #               'panels.quick_sculpt_mode']

modulesNames = ['panels.quick_object_mode']
modulesNames = ['panels.cleanup_panel']

modulesFullNames = {}
for currentModuleName in modulesNames:
    if 'DEBUG_MODE' in sys.argv:
        modulesFullNames[currentModuleName] = ('{}'.format(currentModuleName))
    else:
        modulesFullNames[currentModuleName] = ('{}.{}'.format(__name__, currentModuleName))

for currentModuleFullName in modulesFullNames.values():
    if currentModuleFullName in sys.modules:
        importlib.reload(sys.modules[currentModuleFullName])
        print("Importing module: " + currentModuleFullName)
    else:
        globals()[currentModuleFullName] = importlib.import_module(currentModuleFullName)
        setattr(globals()[currentModuleFullName], 'modulesNames', modulesFullNames)


def register():
    for currentModuleName in modulesFullNames.values():
        if currentModuleName in sys.modules:
            if hasattr(sys.modules[currentModuleName], 'register'):
                print("Module register: " + currentModuleFullName)

                sys.modules[currentModuleName].register()


def unregister():
    for currentModuleName in modulesFullNames.values():
        if currentModuleName in sys.modules:
            if hasattr(sys.modules[currentModuleName], 'unregister'):
                sys.modules[currentModuleName].unregister()


if __name__ == "__main__":
    register()
