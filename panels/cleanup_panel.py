import bpy
from bpy.props import IntProperty
from bl_ui.properties_paint_common import UnifiedPaintPanel


### Feature set:
##### Texture auto hot-reload
#####  setup (solidify + subsurf + mirror and so on...)
##### Unused textures cleanup
##### My own flavour of Quicktools (bind to Q)

class WarpToolsPopup(bpy.types.Operator, UnifiedPaintPanel):
    bl_idname = "view3d.basic_settings"
    bl_label = "Brush Basic Settings"

    def execute(self, context):
        print("WarpToolsPopup execute")

        return context.window_manager.invoke_popup(self, width=300)

    # return context.output.invoke_popup(self, width=300)

    def draw(self, context):
        print("WarpToolsPopup")

        scene = context.scene
        layout = self.layout

        box = layout.box()
        # box.operator("warp.mesh_cleanup_run_all"
        box.operator("warp.mesh_cleanup_geometry_run_all")
        box.operator("warp.mesh_cleanup_run_all")


class WarpTools_Utilities_Panel(bpy.types.Panel):
    bl_label = "Warp Cleanup Tools"
    bl_idname = "cod.xmodel_export"
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "output"

    def execute(self, context):
        print("WarpTools_Utilities_Panel execute")
        # return context.window_manager.invoke_popup(self, width=300)
        return {'FINISHED'}

    def mesh_cleanup_draw(self, context, layout):
        scene = context.scene
        row = layout.row()

        row.operator("warp.mesh_cleanup_run_all")
        row = layout.row()
        row.operator("warp.mesh_cleanup_remove_parent")
        row.operator("warp.mesh_cleanup_rename_uvs")
        row.operator("warp.mesh_cleanup_apply_transform")

        box = layout.box()
        # row = box.row()
        # row.label(text="asd", icon = 'X')

        ### Options
        # box = box.box()
        if scene.show_mesh_geometry_cleanup_options:
            box.prop(scene, "show_mesh_geometry_cleanup_options", text="Geometry Cleanup", icon="TRIA_DOWN",
                     emboss=False)

            row = box.row()
            row.operator("warp.mesh_cleanup_geometry_run_all")

            # Delete loose.
            row = box.row()
            row.prop(scene, "geometry_cleanup_invoke_delete_loose")
            row.operator("warp.mesh_cleanup_delete_loose")

            # Decimate.
            row = box.row()
            row.prop(scene, "geometry_cleanup_invoke_decimate")
            row.operator("warp.mesh_cleanup_decimate")

            # Degenerate Dissolve.
            row = box.row()
            row.prop(scene, "geometry_cleanup_invoke_degenerate_dissolve")
            row.operator("warp.mesh_cleanup_degenerate_dissolve")

            # Limited Dissolve.
            # row = box.row()
            # row.prop(scene, "geometry_cleanup_invoke_limited_dissolve")
            # row.operator("mesh.dissolve_limited")

            # Make non-planar faces.
            # row = box.row()
            # row.prop(scene, "geometry_cleanup_invoke_make_nonplanar_faces")
            # row.operator("mesh.vert_connect_nonplanar")

            # Split non-planar faces.
            # row = box.row()
            # row.prop(scene, "geometry_cleanup_invoke_split_nonplanar")
            # row.operator("mesh.vert_connect_nonplanar")

            # Split concave faces.
            row = box.row()
            row.prop(scene, "geometry_cleanup_invoke_split_concave")
            row.operator("warp.mesh_cleanup_split_concave")

            # Remove doubles.
            row = box.row()
            row.prop(scene, "geometry_cleanup_invoke_remove_doubles")
            row.operator("warp.mesh_cleanup_remove_doubles")

            # Fill holes.
            row = box.row()
            row.prop(scene, "geometry_cleanup_invoke_fill_holes")
            row.operator("warp.mesh_cleanup_fill_holes")



        else:
            box.prop(scene, "show_mesh_geometry_cleanup_options", icon="TRIA_RIGHT", emboss=False)

    def materials_cleanup_draw(self, context):
        layout = self.layout
        row = layout.row()

        unused_materials_count = 0
        for material in bpy.data.materials:
            if not material.users:
                unused_materials_count += 1

        title = "Purge "
        if unused_materials_count > 0:
            title += str(unused_materials_count)
        title += " unused material"
        if unused_materials_count > 1:
            title += "s"

        if unused_materials_count == 0:
            row.enabled = False
            title = "No unused materials to purge"

        row.operator("warp.materials_cleanup", text=title)

    def draw(self, context):
        layout = self.layout
        scene = context.scene

        self.materials_cleanup_draw(context)

        box = layout.box()

        self.mesh_cleanup_draw(context, box)

    # row = layout.row()
    # row.label(text="XModels", icon="CUBE")


class CleanupMaterials(bpy.types.Operator):
    bl_idname = "warp.materials_cleanup"
    bl_label = "Purge unused materials"
    bl_options = set()

    def execute(self, context):
        print("Removing all unused materials...")

        for material in bpy.data.materials:
            if not material.users:
                bpy.data.materials.remove(material)
        return {'FINISHED'}


class MeshCleanupGeometryAll(bpy.types.Operator):
    bl_idname = "warp.mesh_cleanup_geometry_run_all"
    bl_label = "Invoke all geometry cleanup"
    bl_options = set()

    def execute(self, context):
        print("Running all mesh geometry cleanup procedures...")
        bpy.ops.warp.materials_cleanup('INVOKE_DEFAULT')
        bpy.ops.warp.mesh_cleanup_remove_parent('INVOKE_DEFAULT')
        bpy.ops.warp.mesh_cleanup_rename_uvs('INVOKE_DEFAULT')
        bpy.ops.warp.mesh_cleanup_apply_transform('INVOKE_DEFAULT')
        return {'FINISHED'}


class MeshCleanupRunAll(bpy.types.Operator):
    bl_idname = "warp.mesh_cleanup_run_all"
    bl_label = "Invoke all cleanup procedures"
    bl_options = set()

    def execute(self, context):
        print("Running all mesh cleanup procedures...")

        scene = context.scene
        if scene.geometry_cleanup_invoke_delete_loose:
            bpy.ops.warp.mesh_cleanup_delete_loose('INVOKE_DEFAULT')
        if scene.geometry_cleanup_invoke_decimate:
            bpy.ops.warp.mesh_cleanup_decimate('INVOKE_DEFAULT')
        if scene.geometry_cleanup_invoke_degenerate_dissolve:
            bpy.ops.warp.mesh_cleanup_degenerate_dissolve('INVOKE_DEFAULT')
        if scene.geometry_cleanup_invoke_split_concave:
            bpy.ops.warp.mesh_cleanup_split_concave('INVOKE_DEFAULT')
        if scene.geometry_cleanup_invoke_remove_doubles:
            bpy.ops.warp.mesh_cleanup_remove_doubles('INVOKE_DEFAULT')
        if scene.geometry_cleanup_invoke_fill_holes:
            bpy.ops.warp.mesh_cleanup_fill_holes('INVOKE_DEFAULT')

        print("Cleanup Run All done.")
        return {'FINISHED'}


class MeshCleanupRemoveParentTransform(bpy.types.Operator):
    bl_idname = "warp.mesh_cleanup_remove_parent"
    bl_label = "Remove parent"
    bl_options = set()

    def execute(self, context):
        print("Mesh cleanup: remove parent keeping transform.")
        return {'FINISHED'}


class MeshCleanupRenameUV(bpy.types.Operator):
    bl_idname = "warp.mesh_cleanup_rename_uvs"
    bl_label = "Rename UVs"
    bl_options = set()

    def execute(self, context):
        print("Mesh cleanup: rename uvs.")
        return {'FINISHED'}


class MeshCleanupApplyTransform(bpy.types.Operator):
    bl_idname = "warp.mesh_cleanup_apply_transform"
    bl_label = "Apply all transforms"
    bl_options = set()

    def execute(self, context):
        print("Mesh cleanup: apply position, rotation and scale.")
        return {'FINISHED'}


## Geometry cleanup wwappers.

class MeshCleanupDeleteLoose(bpy.types.Operator):
    bl_idname = "warp.mesh_cleanup_delete_loose"
    bl_label = "Delete Loose Geo"

    def execute(self, context):
        print("Mesh cleanup: delete loose geometry.")
        return {'FINISHED'}


class MeshCleanupDecimate(bpy.types.Operator):
    bl_idname = "warp.mesh_cleanup_decimate"
    bl_label = "Decimate Geometry"

    def execute(self, context):
        print("Mesh cleanup: decimate geometry.")
        return {'FINISHED'}


class MeshCleanupDegenerateDissolve(bpy.types.Operator):
    bl_idname = "warp.mesh_cleanup_degenerate_dissolve"
    bl_label = "Degenerate Dissolve"

    def execute(self, context):
        print("Mesh cleanup: degenerate dissolve.")
        return {'FINISHED'}


class MeshCleanupLimitedDissolve(bpy.types.Operator):
    bl_idname = "warp.mesh_cleanup_limited_dissolve"
    bl_label = "Limited Dissolve"

    def execute(self, context):
        print("Mesh cleanup: limited dissolve.")
        return {'FINISHED'}


class MeshCleanupMakePlanarFaces(bpy.types.Operator):
    bl_idname = "warp.mesh_cleanup_make_planar_faces"
    bl_label = "Make Planar Faces"

    def execute(self, context):
        print("Mesh cleanup: make planar faces.")
        return {'FINISHED'}


class MeshCleanupSplitNonPlanar(bpy.types.Operator):
    bl_idname = "warp.mesh_cleanup_split_nonplanar"
    bl_label = "Split Non-Planar Faces"

    def execute(self, context):
        print("Mesh cleanup: split non-planar faces.")
        return {'FINISHED'}


class MeshCleanupSplitConcave(bpy.types.Operator):
    bl_idname = "warp.mesh_cleanup_split_concave"
    bl_label = "Split Concave Faces"

    def execute(self, context):
        print("Mesh cleanup: split concave faces.")
        return {'FINISHED'}


class MeshCleanupRemoveDoubles(bpy.types.Operator):
    bl_idname = "warp.mesh_cleanup_remove_doubles"
    bl_label = "Remove doubles"

    def execute(self, context):
        print("Mesh cleanup: remove doubles.")
        return {'FINISHED'}


class MeshCleanupFillHoles(bpy.types.Operator):
    bl_idname = "warp.mesh_cleanup_fill_holes"
    bl_label = "Fill Holes"

    def execute(self, context):
        print("Mesh cleanup: fill holes.")
        return {'FINISHED'}


# class CleanupHiddenObjects(bpy.types.Operator):


def register():
    for cls in (WarpTools_Utilities_Panel, CleanupMaterials, MeshCleanupRunAll, MeshCleanupApplyTransform,
                MeshCleanupRemoveParentTransform, MeshCleanupRenameUV, MeshCleanupGeometryAll, MeshCleanupDeleteLoose,
                MeshCleanupDecimate, MeshCleanupDegenerateDissolve, MeshCleanupLimitedDissolve,
                MeshCleanupMakePlanarFaces, MeshCleanupSplitNonPlanar, MeshCleanupSplitConcave,
                MeshCleanupRemoveDoubles, MeshCleanupFillHoles, WarpToolsPopup):
        bpy.utils.register_class(cls)
    # bpy.utils.register_class(WarpTools_Utilities_Panel)

    bpy.types.Scene.show_mesh_geometry_cleanup_options = bpy.props.BoolProperty(name='All Cleanup Options',
                                                                                default=False)
    bpy.types.Scene.geometry_cleanup_invoke_delete_loose = bpy.props.BoolProperty(name='Delete Loose Geometry',
                                                                                  default=False)
    bpy.types.Scene.geometry_cleanup_invoke_decimate = bpy.props.BoolProperty(name='Decimate Geometry', default=False)
    bpy.types.Scene.geometry_cleanup_invoke_degenerate_dissolve = bpy.props.BoolProperty(name='Degenerate Dissolve',
                                                                                         default=False)
    bpy.types.Scene.geometry_cleanup_invoke_limited_dissolve = bpy.props.BoolProperty(name='Limited Dissolve',
                                                                                      default=False)
    bpy.types.Scene.geometry_cleanup_invoke_make_nonplanar_faces = bpy.props.BoolProperty(name='Make Planar Faces',
                                                                                          default=False)
    bpy.types.Scene.geometry_cleanup_invoke_split_nonplanar = bpy.props.BoolProperty(name='Split Non-planar Faces',
                                                                                     default=False)
    bpy.types.Scene.geometry_cleanup_invoke_split_concave = bpy.props.BoolProperty(name='Split Concave Faces',
                                                                                   default=False)
    bpy.types.Scene.geometry_cleanup_invoke_remove_doubles = bpy.props.BoolProperty(name='Remove doubles',
                                                                                    default=False)
    bpy.types.Scene.geometry_cleanup_invoke_fill_holes = bpy.props.BoolProperty(name='Fill holes', default=False)


if __name__ == "__main__":
    register()