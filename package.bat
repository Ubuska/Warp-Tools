@echo off
title Package
echo Ossim package process started
if not exist "%~dp0\ossim" mkdir %~dp0\ossim
copy "%~dp0\__init__.py" "%~dp0\ossim"
copy "%~dp0\panel_ossim.py" "%~dp0\ossim"


if not exist "%~dp0\ossim\panels" mkdir %~dp0\ossim\panels
copy "%~dp0\panels\." "%~dp0\ossim\panels"
if not exist "%~dp0\ossim\operators" mkdir %~dp0\ossim\operators
copy "%~dp0\operators\." "%~dp0\ossim\operators"

7z.exe a - tzip Test.zip %~dp0\ossim

pause